Associatie detectie in datasets.
==============================================================================

Heb je een (grote) ongestructureerde dataset en wil je de data gaan verkennen?
Dan is de start met het **HNet-Dashboard** een goede start!

`Klik hier om naar het HNet-Dashboard te gaan <https://rwsdatalab.gitlab.io/public/codebase/dashboards/hnet-dashboard/hnet/>`_

.. _schematic_overview:

.. figure:: ./figs/hnet_explore_2.png




.. toctree::
   :maxdepth: 1
   :caption: Algemeen

   abstract.rst


.. toctree::
   :maxdepth: 1
   :caption: Dashboard

   read_data.rst

.. toctree::
   :maxdepth: 1
   :caption: Resultaten

   output.rst


.. toctree::
   :maxdepth: 1
   :caption: Save en load

   Save and Load.rst


.. toctree::
   :maxdepth: 1
   :caption: Notes

   Privacy.rst

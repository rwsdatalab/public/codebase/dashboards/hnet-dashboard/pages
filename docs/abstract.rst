Toepassing
#############

De stap van **data** naar **inzichten** naar **kennis** zijn niet altijd logische eenduidige stappen. Er zijn veel factoren die hierop invloed hebben, denk bijvoorbeeld aan het aantal variabelen in de dataset, maar ook of het *continuous*, *discreet*, en/of *categorical* waardes zijn.

Op het moment dat er geen aannames gemaakt (kunnen) worden over de variabelen, dan kan de **verkennende fase** extreem tijdrovend en uitdagend zijn. Dit komt omdat de zoekruimte super-exponentieel wordt in het aantal variabelen. Of anders gezegd, de combinaties van variabelen die je kan maken zijn er te veel om met de hand te doen.

Om te kunnen voorzien in de verkennende fase hebben wij een dashboard ontwikkeld voor de methode `Graphical Hypergeometric Networks (HNET) <https://arxiv.org/abs/2005.04679>`_. Deze methode toetst of er significante relaties zijn tussen de variabelen. Zodoende kan het hints en aanwijzingen geven over de relaties tussen de variabelen, ongeacht het *continuous*, *discreet*, en/of *categorical* waardes zijn.

De output van het model toont een netwerk van significante relaties dat vervolgens (in het dashboard) nog verder gefilterd kan worden. Het dashboard is zodanig ontwikkeld dat geen programmeerkennis vereist is om de ruwe ongestructureerde datasets te *uploaden*, *voorkeur variabelen te selecteren*, *verwijderen/filteren* van ongewenste gegevens en vervolgens te *analyseren*.


Verkennende fase
##########################

Het **HNet-dashboard** is bedoeld voor de verkennende fase in een project. De relaties tussen variabelen zijn zogenoemde **associaties** en hebben **niet** persee een **causaal** verband. Het advies is daarom ook dat de verkregen inzichten niet leidend moeten zijn; het zijn slechts **aanwijzingen** om vervolg analyses uit te voeren.


Schematische weergave
##########################

Een schematische weergave van het model is hieronder weergegeven.
* Arikel op `Arxiv <https://arxiv.org/abs/2005.04679>`_.
* Blog op `Medium <https://towardsdatascience.com/explore-and-understand-your-data-with-a-network-of-significant-associations-9a03cf79d254>`_.



.. |figA1| image:: ./figs/schematic_overview.png

.. table:: Schematische weergave.
   :align: center

   +----------+
   | |figA1|  |
   +----------+

################################################################################
pages
################################################################################

.. begin-inclusion-intro-marker-do-not-remove



.. end-inclusion-intro-marker-do-not-remove


.. begin-inclusion-usage-marker-do-not-remove

How to use
----------

Provide user documentation here.

.. end-inclusion-usage-marker-do-not-remove


.. begin-inclusion-installation-marker-do-not-remove

Installation
------------

To install pages, do:

.. code-block:: console

  git clone https://gitlab.com/rwsdatalab/codebase-opensource/dashboards/hnet-dashboard/pages/pages.git
  cd pages
  pip install .

Run tests (including coverage) with:

.. code-block:: console

  pip install -r requirements-dev.txt
  python setup.py test

.. end-inclusion-installation-marker-do-not-remove


Documentation
-------------

.. _README:

Include a link to your project's full documentation here.


.. begin-inclusion-license-marker-do-not-remove

License
-------

Copyright (c) 2022, Rijkswaterstaat


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



.. end-inclusion-license-marker-do-not-remove
